﻿using System;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;

public class SortChildObjects : EditorWindow {
    private enum ESortType {
        Alpha,
        X,
        Y,
        Z
    };

    [MenuItem("GameObject/Sort/By Name", false, -1)]
    public static void SortGameObjectsByName (MenuCommand menuCommand) {
        SortGameObjects(menuCommand);
    }

    [MenuItem("GameObject/Sort/By Name Reverse", false, -1)]
    public static void SortGameObjectsByNameReverse (MenuCommand menuCommand) {
        SortGameObjects(menuCommand, ESortType.Alpha, true);
    }

    //NMP [MenuItem("GameObject/Sort/By X position", false, -1)]
    public static void SortGameObjectsByX (MenuCommand menuCommand) {
        SortGameObjects(menuCommand, ESortType.X);
    }

    //NMP [MenuItem("GameObject/Sort/By X position Reverse", false, -1)]
    public static void SortGameObjectsByXReverse (MenuCommand menuCommand) {
        SortGameObjects(menuCommand, ESortType.X, true);
    }

    private static void SortGameObjects (MenuCommand menuCommand, ESortType sortType = ESortType.Alpha, bool reverse = false) {
        if (menuCommand.context == null || menuCommand.context.GetType() != typeof(GameObject)) {
            EditorUtility.DisplayDialog("Error", "To use this command, You must select an item in the hierachy panel and use right clic", "Okay");
            return;
        }

        GameObject parentObject = (GameObject) menuCommand.context;
        if (parentObject.GetComponentInChildren<Image>()) {
            EditorUtility.DisplayDialog("Error", "You are trying to sort a GUI element. This will screw up EVERYTHING, do not do", "Okay");
            return;
        }

        // Build a list of all the Transforms in this player's hierarchy
        var objectTransforms = new Transform[parentObject.transform.childCount];
        for (var i = 0; i < objectTransforms.Length; i++)
            objectTransforms[i] = parentObject.transform.GetChild(i);

        var sortTime = Environment.TickCount;

        var sorted = false;
        // Perform a bubble sort on the objects
        while (!sorted) {
            sorted = true;
            for (var i = 0; i < objectTransforms.Length - 1; i++) {
                bool comparisonTest;
                float comparison;
                switch (sortType) {
                    case ESortType.Alpha:
                        // Compare the two strings to see which is sooner
                        comparison = string.Compare(objectTransforms[i].name, objectTransforms[i + 1].name, StringComparison.Ordinal);

                        break;
                    case ESortType.X:
                        comparison = objectTransforms[i+ 1].position.x - objectTransforms[i ].position.x;
                        break;
                    case ESortType.Y:
                        comparison = objectTransforms[i+ 1].position.y - objectTransforms[i ].position.y;
                        break;
                    case ESortType.Z:
                        comparison = objectTransforms[i+ 1].position.z - objectTransforms[i ].position.z;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException("sortType", sortType, null);
                }

                if (reverse)
                    comparisonTest = (comparison < 0); // 1 means that the current value is larger than the last value
                else
                    comparisonTest = (comparison >= 0); // 1 means that the current value is larger than the last value

                if (comparisonTest) {
                    objectTransforms[i].transform.SetSiblingIndex(objectTransforms[i + 1].GetSiblingIndex());
                    sorted = false;
                }
            }

            // resort the list to get the new layout
            for (var i = 0; i < objectTransforms.Length; i++)
                objectTransforms[i] = parentObject.transform.GetChild(i);
        }

        Debug.Log("Sort took " + (Environment.TickCount - sortTime) + " milliseconds");
    }
}
